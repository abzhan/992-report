\documentclass[10pt, journal]{IEEEtran}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage[pdftex]{graphicx}
\graphicspath{{img/}}
\DeclareGraphicsExtensions{.pdf,.jpeg,.png}

\ifCLASSOPTIONcompsoc
  \usepackage[caption=false,font=normalsize,labelfont=sf,textfont=sf]{subfig}
\else
  \usepackage[caption=false,font=footnotesize]{subfig}
\fi

\usepackage{balance}
\usepackage{color}
\usepackage[normalem]{ulem}


\begin{document}

\title{Improving Regression Based Landmark 
\\Detection in CT Images of the Upper Airway}

\author{
\IEEEauthorblockN{Aidos Abzhanov $^*$\\}
\IEEEauthorblockA{Department of Computer Science\\
University of North Carolina at Chapel Hill\\
Chapel Hill, NC\\
Email: aidos@cs.unc.edu
\thanks{* This work was done under the supervision of Professor Marc Niethammer.}
}
%\and
%\IEEEauthorblockN{Marc Niethammer}
%\IEEEauthorblockA{Department of Computer Science\\
%University of North Carolina at Chapel Hill\\
%Chapel Hill, NC\\
%Email: mn@cs.unc.edu}
}

\maketitle



\begin{abstract}
Anatomical landmark detection plays an important role in medical image analysis. By providing better tools for the detection of landmarks in the upper airway, one could possibly  help improve the diagnosis of such airway problems as subglottic stenosis and tracheobronchomalacia. The state of the art in landmark detection is using regression forests along with regression voting. However, the detector is constrained to find locations only inside the image, even if the true location of a landmark is outside. Moreover, there is a non-zero chance that the detector would find a location that is far enough from the true one that it cannot be used for subsequent analysis. To address these issues, the following two methods were proposed that modify the regression-based landmark detection pipeline.  The first method removes the restriction on a voting area and can distinguish landmarks that are inside the image from those that are outside it. The second method combats detection outliers by restricting detections to plausible areas. The proposed methods are supported by experimental results on CT images of the upper airway of pediatric patients.
\end{abstract}



% I N T R O D U C T I O N  ( and background )
\section{ Introduction }

Many medical image processing and analysis tasks require the knowledge of anatomical landmarks, distinct locations in the image that characterize local anatomy (Figure~\ref{fig:airway}). For example, image registration methods use landmarks  to match images and measure image alignment \cite{johnson2002consistent}, and in shape analysis methods landmarks initialize deformable models \cite{zhang2012towards}. Depending on the application, landmarks can be identified manually or automatically. Manual landmark detection is very flexible and can be easily adapted to data \cite{hill1994accurate}. However, it is usually time-consuming to find true 3D landmarks in an image. The problem becomes more pronounced for dynamic, 4D, computed tomography (CT) images which require the processing of a series of 3D images. Automatic landmark detection can easily handle 4D CT images in the same way as for 3D CT images, but it faces a challenge of consistency of landmark spatial localization across different subjects and image types  \cite{worz2006localization}. There are two main approaches to automatic landmark detection: classification-based and regression-based. In classification-based methods, typically, a small neighborhood centered at the landmark is marked as foreground, with the rest of the image being marked as background. Then a binary classification problem is solved by training a strong classifier to distinguish the foreground voxels from the background. For example, this type of automatic landmark detection was used in \cite{cheng2011automatic} for cone-beam CT dental images and in \cite{zhan2011robust} for MR knee images. Regression-based methods train regression forests to learn non-linear correspondences between the local image appearance and the displacements to the target landmark. The subsequent voting procedure determines the final position of the landmark. Generally, regression-based methods outperform classification-based detectors \cite{cootes2012robust}.  This work concentrates on the regression-based approach.

\begin{figure}[!t]
\centering
\includegraphics[width=2.5in]{Airway}
\caption{A slice of CT image, showing an upper airway (dark color) along with 
some anatomical landmarks (pink dots)}
\label{fig:airway}
\end{figure}

The CT imaging of upper airways is one type of medical imaging that would potentially benefit from robust and accurate automatic  landmark detection. For example, \cite{hong2013pediatric} and \cite{zdanski2015quantitative} proposed a quantitative, non-invasive assessment of subglottic stenosis (SGS) by constructing a normal control airway atlas that captures airway geometry and its variation. Accurately detected landmarks are crucial to performing registration and establishing a common atlas space. Additionally, landmark detection in airway images can potentially be applied in methods to distinguish between subjects with tracheobronchomalacia (TBM) and normal subjects.

\begin{figure*}[!t]
\centering
\subfloat[A slice of 3D CT image with airway landmarks. The Trachea Carina is the lowest landmark in the image.]{
	\includegraphics[width=2.0in]{original_1074}
	\label{fig:original_1074} }
\hfil
\subfloat[Slice of a cropped CT image. The Trachea Carina is now located outside of the image.]{
	\includegraphics[width=2.0in]{cropped_1074}
	\label{fig:cropped_1074} }
\caption{Original CT image and its cropped version.}
\label{fig:original-cropped}
\end{figure*}

This paper will focus on improving the regression forest-based landmark detection methods for 3D and 4D CT images of airways in pediatric patients. The first problem that I am going to address is that a given CT image may not contain the full set of landmarks that one is expecting to detect. This  may result in false landmark detection, which can lead to undesired consequences later when the landmark is used in analysis. Thus, it is preferable to determine beforehand if a given landmark is present in the image or not. The second problem is that independent landmark detection is not always reliable. A voting map covers the whole image, and a distant location can be chosen as an outcome of the voting. This results in detection outliers (see Figure~\ref{fig:boxplot-original}).
%some landmarks are more difficult to detect reliably, even though the general landmark localization is overall good (Fig. \ref{fig:boxplot-original}).

I will show how the regression-based landmark detection pipeline can be changed to address these problems. I start with background on regression forests for landmark detection and describe how to extract an airway segmentation from 3D CT images. In subsequent sections I present the methodology and experiments conducted. I then discuss the obtained results and potential future work.


% REGRESSION FOREST
\subsection{ Regression Forest for Landmark Detection }

\begin{figure*}[!t]
\centering
\subfloat[Voting map for a landmark that is contained in the image.]{
	\includegraphics[width=2.0in]{VotingMapOnFullImage}
	\label{fig:vmfull} }
\hfil
\subfloat[Voting map for a landmark that is not contained in the image.]{
	\includegraphics[width=2.0in]{VotingMapOnCroppedImage}
	\label{fig:vmcropped} }
\caption{Volume rendering of a voting map, projected on a slice of 3D CT image.  As the value on the voting map increases, the color changes from red to yellow.}
\label{fig:vm}
\end{figure*}

Shortly after their successful application to computer vision problems \cite{viola2004robust}, random forests gained popularity among medical imaging researchers due to their robustness and accuracy \cite{criminisi2010regression}. One type of random forest that is  particularly efficient for the task of anatomical feature localization is the regression forest. The training and detection technique, which establishes a relationship between image voxels and anatomical features, has been applied to various medical imaging problems, e.g. deformable model initialization in prostate computed tomography (CT) images \cite{gao2014context, dai2015online} and in magnetic resonance (MR) brain image registration \cite{han2014robust}.

The general regression-guided framework employed for landmark detection in 3D images is the following. A regression forest learns a highly non-linear correspondence between the local image appearance and the displacements to the target landmarks. A popular choice to characterize the local image appearance are Haar-like features \cite{criminisi2013regression}. For each voxel $\textbf{v}$, the Haar-like features have the following form:
$$
%{\color{red} 
	v(\textbf{v}) = \sum_{i=0}^Z p_i 
	\sum_{|| \textbf{x} - a_i ||_\infty \leq s_i} I(\textbf{x}),
%	}
$$
where $Z$ is the number of used 3D cubic functions and each 3D cubic function has polarity $p_i \in \{-1, 1\}$, position $a_i \in \mathbb{R}^3$ relative to $\mathbf{v}$, and scale $s_i$. Typical values are $\{1,2\}$ for $Z$, $\{3,5\}$ for $s$, and $a$ is confined to a $30 \times 30 \times 30$ cube centered in $\textbf{v}$.

The detection of a landmark on a test image is performed by regression voting \cite{lindner2015robust}: accumulating the votes across all image voxels and then selecting the voxel with the highest number of votes.
 
 
% AIRWAY SEGMENTATION
\subsection{ Airway segmentation and centerline }
\label{sec:airway_segmentation}
Extraction of airway geometry from CT images and its characterization is critical for this project. I used a simplified airway model developed by Hong, et al. in \cite{hong2013pediatric}. This model consists of airway geometry segmentation and a centerline with cross sections. The airway segmentation is obtained in a semi-automatic way, by performing binary Otsu's thresholding method with manually chosen seed points. The centerline is computed by solving the Laplace equation for the heat distribution along the airway and combining the centers of the iso-surfaces of the obtained solution. I discuss the usefulness of the centerline for generating a set of extended landmarks in section ~\ref{sec:diss}.




% M E T H O D S
\section{ Methods }		

\subsection{ Detecting landmarks outside of an image }
As pointed out earlier, regression-based landmark detection provides good localization results \cite{lindner2015robust}. The typical regression-based detection pipeline that is employed in contemporary research articles is implemented in a multi-resolution way \cite{gao2014context, dai2015online, han2014robust}. This increases the efficiency of both training and testing stages and improves detection accuracy. In the multi-resolution strategy, the image is processed at different resolutions in a local neighborhood of a landmark. This limits the influence of distant voxels on training and detection results, as they usually introduce noise in the final result.

The fact that the voting procedure shoots voting points at arbitrary locations in space suggests that the detection pipeline can be adapted to decide if a landmark is present in the image. There are a number of issues that need to be considered in order to adapt the detector for landmarks that are outside of an image.

First, the multi-resolution approach to detection is no longer applicable in this case. This can be seen from the more close revisiting of the pipeline. In the multi-resolution approach, the image is processed in a sequence of different resolutions, from coarsest to finest. The landmark detected at a given resolution is used as an initial location guess in the next resolution, and the regression voting is performed only in a local neighborhood of this initialization. For the landmarks that are located outside of an image, this means that, as soon as the coarse level detector predicts a location outside of an image, the behavior of the fine level detector is not determined because the local neighborhood contains points that do not belong to the image. Thus, the original single resolution approach must be used.

\begin{figure*}[!t]
\centering
\subfloat[Airway segmentation computed from a CT image]{
	\includegraphics[width=3.5in]{AirwaySegmented}
	\label{fig:airseg}}
\hfil
\subfloat[Voting map for the Base of Tongue landmark shows multiple local extrema. The location of the global maximum is circled in blue. The true location of the landmark is circled in red. A projection of the airway segmentation is also shown in the image ]{
	\includegraphics[width=2.5in]{double-peaks}
	\label{fig:double-peaks}}
\caption{Airway Segmentation and Voting Map}
\end{figure*}

Second, the restriction on voting only inside a testing image need to be lifted; otherwise, the detector will never predict locations outside of the image. However, in this case, the bounding box that contains voting points is not know in advance. This will complicate the calculation of the voting map and its extremum. To solve this problem, I use hash tables to count votes for each potential location. If needed, the hash table can then be used to create a voting map image. Figure~\ref{fig:vmcropped} show an example of the voting map, generated around the Trachea Carina.

Finally, the location of a landmark is determined by comparing the cumulative weight of votes outside of an image to the ones inside it. I present the results of detecting landmarks outside of an image in section~\ref{sec:results}.


\subsection{ Restricting detection by airway segmentation }
Regression forests are a powerful tool for automatic landmark detection. Figure~\ref{fig:boxplot-original} illustrates the results of detecting 12 landmarks in 25 different CT images. Even though the detection results are good overall, there are still landmarks that were detected too far away from the true landmark locations. One of the reasons this might happen is that the voting map may contain multiple local maxima but the location of its global maximum does not match the true location of the landmark. This case is illustrated in Figure~\ref{fig:double-peaks}. The figure represents a voting map color-coded as blue-black-white for its low to high values. There are two distinct local maxima in the voting map. One maximum, circled in red, aligns with the true location of the landmark. The location that corresponds to the second maximum, circled in blue, is also the global maximum of the voting map and thus is returned as the result of the detection.

In order to address this issue and improve the reliability of detection outcome, I modified the general  framework by restricting the regression voting strategy. This restriction is expressed in the following: I allow voxels to vote anywhere within an image but discard votes that are outside of a plausible area. I use the airway segmentation from \cite{hong2013pediatric} as a mask to define the plausible area. Figure~\ref{fig:airseg} shows an airway segmentation obtained by this method. In contrast to the previous subsection, here I assume that all the landmarks are contained within the image, so I can safely use a multi-resolution approach. At each scale of the detection, I also resample the segmentation mask in addition to the corresponding CT image. To increase the robustness of the detector,  the mask is dilated by one voxel  in each direction.
% before applying it to the voting map. 
 The dilation is performed after each resampling step.

The next section provides the results of applying this method to improve the reliability of automatic landmark detection.



% E X P E R I M E N T S
\section{ Experimental Results }
\label{sec:results}
To validate the proposed methods of landmark detection, I performed two sets of experiments. In the first set of experiments, I tested the ability of the augmented detector to locate landmarks that are outside of an image. The second set of experiments tested  the effect of aiding a landmark detector with an airway segmentation mask.
 
The dataset consisted of 96 upper airway CT images of pediatric patients. Each image had a set of anatomical landmarks that had been manually annotated by an anatomy expert and were considered as ground truth in the analysis. In total, there are 12 distinct landmarks corresponding to different parts of the airway (see Figure~\ref{fig:box-plot} for the names). I used 71 out of 96 images for training and the rest for testing. The training images and their corresponding manually labeled landmarks were used to learn the regression forest. I ran multi-resolution training with 3 distinct scales: 4mm, 2mm, and 1mm spacings for coarse, middle, and fine levels, respectively. Both the training and detection were performed on the UNC KillDevil computing cluster. The regression forest program (C++) from \cite{gao2014context} was used for training and detection, and the airway segmentation dataset was generated using a program (MATLAB) from \cite{hong2013pediatric} . All additional source code was written in C++ and Python.

\begin{figure*}[!t]
\centering
\subfloat[Original detection performance.]{
\includegraphics[width=3.2in]{boxplot-original}
\label{fig:boxplot-original}}
\hfil
\subfloat[Segmentation-guided detection performance]{
\includegraphics[width=3.65in]{boxplot-seg2}
\label{fig:boxplot-seg}}
\caption{The difference between detected and expert labeled landmark locations in mm}
\label{fig:box-plot}
\end{figure*}

\subsection{Detection of landmarks outside of an image}
I chose the Trachea Carina as the main landmark to test the new detection method. In the dataset only 16 out of 25 test images contained the Trachea Carina. I prepared the test images for detection by cropping them (Figure~\ref{fig:original-cropped}), so their size along the z-axis decreased. By varying the size of the cropping, I adjusted the distance between the landmark and its closest image boundary. To analyze what impact this distance may have on detection results, for each test image, I made 10 separate cropped copies where cropping size varied between 5\% and 50\%. For each cropped copy I recorded whether the true landmark location was inside of the image or not, the distance from the true landmark location to the boundary of the cropped image, and the size of the cropped image.

I tested the detection of Trachea Carina on the coarsest scale for all the cropped images as well as on all the original images. I computed the ratio of the number of votes placed outside of the testing image to the number of votes placed inside. I also recorded the detected location of the landmark and its Euclidean distance from the true landmark location.

The detection results are given in Table~\ref{tab:table} and  Figure~\ref{fig:plot}. The table represents each of 16 testing images as a group of 4 rows, and the columns specify the size of applied cropping. If the true landmark is located outside of an image, the distance to the image is given as negative. The image size is the size of CT image in the z-direction. The voting ratio is the ratio between the number of votes outside and inside an image.
 
Figure~\ref{fig:plot} plots the voting ratios against the cropping size. Each piecewise-linear function corresponds to one of the testing images. The line is solid if the true landmark is outside of the image and dashed otherwise.

\subsection{Using segmentation to guide the detection}
The segmentation-guided detection was tested on the same 25 images. I first performed the standard landmark detection without segmentation. The results are given as box plots in Figure~\ref{fig:boxplot-original}. Then I tested the method with segmentation on the same images with the same detection settings. As a segmentation masks I used precomputed airway segmentation images. At each scale of detection this mask was dilated using a $3 \times 3 \times 3$ isotropic matrix. Figure~\ref{fig:boxplot-seg} illustrates the results. In both cases, I performed multi-resolution detection. I will discuss these results in section IV.





% D I S C U S S I O N
\section{ Discussion and Future Work }
\label{sec:diss}
First, I discuss the results of detecting landmarks outside of an image.

The choice of the Trachea Carina as a testing landmark in the experiment was not random; it was chosen for a couple of reasons. First, the landmark has good localization by the original detector. This helps in evaluating the results more reliably.  Second, the Trachea Carina is one of the airway landmarks that is usually located close to the boundary of the image (if present in the image). This allows cropping the part of an image containing the landmark without sacrificing too much of the image size.

The analysis of Figure~\ref{fig:plot} shows that for most of the CT datasets there is a significant difference in the voting ratios when the landmark is present in the image and when it is not.  As long as the landmark is contained within an image, the number of outside votes is negligible compared to the inside ones. The ROC curve in Figure~\ref{fig:roc}  suggests that the optimal threshold value of the voting ratio for distinguishing between outside and inside landmarks  is 0.4543. The corresponding AUC is 0.98.

Figure~\ref{fig:plot} also shows that as the cropping size increases, the voting ratios gradually decrease from their peaks. This happens because as the cropping size increases, so does the distance from the landmark and the image, and thus  the voting map contains more votes from distant voxels. However, as was mentioned earlier, the closer a voxel is located to the true landmark, the better it detects the landmark's location.

\begin{figure*}[!t]
\centering
\includegraphics[width=7in]{plot-05}
\caption{The voting ratio against the size of cropped image}
\label{fig:plot}
\end{figure*}

Even though there are good results in determining whether the landmark is inside or outside of the image, it is still hard to detect the exact location of the landmark correctly. Table~\ref{tab:table} shows that the error of localization significantly increases for the cases where the landmark is not present in the image.

I now discuss the results of the detection with a segmentation mask (Figure~\ref{fig:box-plot}). I consider  different groups of the landmarks separately based on the performance of the detector.

For the following landmarks the proposed method was able to improve the detection by reducing the number of outliers: TVC, Subglottic, Pyrina Aperture, Posterior Inferior Vomer Corner (PIVomerCorner). Landmarks such as Trachea Carina, Epiglottis Tip, and Base of Tongue did not see much change in their detection results. However, a noticeable deterioration can be seen in detecting Right and Left ala Rims, Nose Tip, Nasal Spine, and Columella.

To understand why there is an increase in detection error for the last group of the landmarks, one can notice that those landmarks describe spatially close locations around the human nose. Analyzing the segmentation mask images, one can find that for many of them these are the most distant from the airway landmarks. Because of this, the segmentation mask does not contain the true landmark location, and the detector is likely to pick a more distant location based on the voting map.

One solution to the previous problem might be a dilation of the segmentation image with a wider stencil in the region of interest (local neighborhood of Columella).



\subsection{Future work}
I see this work as opening a number of  possible directions for future research. First, it might be reasonable to perform more experiments to evaluate the proposed methods. Because of the small size of the dataset, it is advisable  to perform a cross-validation check to get a better assessment of the detection outside of an image. The relationship between the size of the dilation stencil and the accuracy of landmark detection is the next issue one might want to analyze.

Second, it still desirable to correctly detect the true location of landmarks that reside outside of an image. One possible approach to this problem is to analyze  the distribution of the votes in the voting map more closely.

Third, the segmentation mask can be moved to the training stage of the regression-based landmark detection pipeline. Currently, to perform the segmentation-guided detection, one has to precompute the segmentation mask for the subject image. This might significantly increase the overall detection time. If one could represent an airway segmentation image as a context during the regression forest training stage and use this context while learning the intensity profile, it would potentially increase the detection accuracy without using a segmentation mask at detection stage. 

The proposed segmentation-guided landmark detection approach might be applicable to detecting an extended set of landmarks. This extended set consists of the landmarks that might not have the distinct appearance but are more reliable to detect computationally than manually chosen ones. A proposed way to generate landmark candidates for the extended set is to sample a large number of equally spaced points along the airway centerline. These points would then be used to train and test regression-based detection. The points that can be  most reliably detected would be included in the extended set. Segmentation masking applied at detection stage might help to better choose the extended set points, as they should always be inside a segmentation mask by construction.

Finally, it may be  interesting to see how the proposed detection methods can be applied to dynamic CT images. Specifically, I am interested in whether it is possible to improve the detected locations of outside landmarks by combining the voting maps from different frames of a dynamic image. The idea of using  segmentation-guided detection to improve the temporal consistency of the landmarks across the frames also looks appealing. As a preliminary step in this direction,  I have implemented an external module for the  3D Slicer program to help evaluating the results for dynamic CT images.
%the  3D Slicer , an open-source platform for medical imaging research

\begin{figure*}[!t]
\centering
\includegraphics[width=4in]{roc_curve2}
\caption{The ROC curve for voting ratio}
\label{fig:roc}
\end{figure*}

% C O N C L U S I O N
\section{ Conclusion  }
In this paper, I have presented a practical approach for modifying an existing regression-based landmark detection methods to reliably determine whether or not a target landmark is inside of a testing image. By allowing the detector to place votes outside of an image and comparing the voting map results, I was able to correctly predict the relative location of the landmark for the majority of the test cases.

The second result is a method for improving the reliability of landmark localization by restricting the votes to a plausible area.  The improvements in detection for some of the landmarks were registered and reported. I also addressed the drawbacks of the method in relation to the group of the landmarks around the human nose.

The paper also discussed a number of possible improvements, extensions, and applications of the proposed methods.

\section*{ Acknowledgment }
The author thanks Y. Hong and Y. Gao for interesting discussions, C. Quammen for providing the dataset of segmented airways, Professor M. Niethammer for the guidance during the project, and Professor S. Pizer for his valuable feedback on this paper.
\balance

% B I B L I O G R A P H Y 
\bibliographystyle{IEEEtran}
\bibliography{references}


% T A B L E
\begin{table*}[!t]
\caption{The results of detecting the Trachea Carina using the proposed method.}
\label{tab:table}
\centering
    \begin{tabular}{ | l | l | l | l | l | l | l | l | l | l | l | l | l | }
    \hline
    
&  0\% & 5\% & 10\% & 15\% & 20\% & 25\% & 30\% & 35\% & 40\% & 45\% & 50\% & \\ \hline \hline
1002 & -22.33 & -14.73 & -7.13 & 0.87 & 8.47 & 16.47 & 24.07 & 31.67 & 39.67 & 47.27 & 55.27  & dist. to img. \\ \hline
 & 155.61 & 148.01 & 140.41 & 132.41 & 124.81 & 116.81 & 109.21 & 101.61 & 93.61 & 86.01 & 78.00  & img. size \\ \hline
 & 0.34 & 0.71 & 1.19 & 1.43 & 1.45 & 1.38 & 1.25 & 1.08 & 1.00 & 0.98 & 0.86  & voting ratio \\ \hline
 & 22.50 & 14.89 & 15.28 & 19.25 & 19.73 & 23.69 & 24.09 & 24.49 & 24.49 & 21.56 & 21.55  & dist. true and detect. \\ \hline \hline
1003 & -14.05 & -5.05 & 6.95 & 18.95 & 30.95 & 42.95 & 51.95 & 63.95 & 75.95 & 87.95 & 99.95  & dist. to img. \\ \hline
 & 231.00 & 222.00 & 210.00 & 198.00 & 186.00 & 174.00 & 165.00 & 153.00 & 141.00 & 129.00 & 117.00  & img. size \\ \hline
 & 0.06 & 0.12 & 0.28 & 0.43 & 0.69 & 1.02 & 1.35 & 1.56 & 1.56 & 1.47 & 1.29  & voting ratio \\ \hline
 & 4.62 & 7.36 & 5.46 & 5.46 & 7.36 & 7.36 & 6.72 & 5.36 & 6.66 & 9.59 & 20.66  & dist. true and detect. \\ \hline \hline
1004 & -11.14 & -0.04 & 11.06 & 22.16 & 33.56 & 44.66 & 55.76 & 67.16 & 78.26 & 89.36 & 100.76  & dist. to img. \\ \hline
 & 223.80 & 212.70 & 201.60 & 190.50 & 179.10 & 168.00 & 156.90 & 145.50 & 134.40 & 123.30 & 111.90  & img. size \\ \hline
 & 0.00 & 0.01 & 0.09 & 0.62 & 1.72 & 2.20 & 2.16 & 2.00 & 1.86 & 1.64 & 1.39  & voting ratio \\ \hline
 & 13.82 & 16.75 & 12.17 & 22.73 & 18.28 & 21.28 & 24.30 & 23.71 & 22.83 & 25.86 & 29.21  & dist. true and detect. \\ \hline \hline
1005 & -54.59 & -43.39 & -32.19 & -20.59 & -9.39 & 2.21 & 13.41 & 24.61 & 36.21 & 47.41 & 59.01  & dist. to img. \\ \hline
 & 227.20 & 216.00 & 204.80 & 193.20 & 182.00 & 170.40 & 159.20 & 148.00 & 136.40 & 125.20 & 113.60  & img. size \\ \hline
 & 0.05 & 0.07 & 0.15 & 0.36 & 0.74 & 1.21 & 1.61 & 1.72 & 1.64 & 1.48 & 1.27  & voting ratio \\ \hline
 & 14.62 & 11.43 & 8.24 & 4.69 & 9.93 & 3.91 & 44.12 & 50.22 & 48.05 & 45.35 & 51.24  & dist. true and detect. \\ \hline \hline
1009 & -44.97 & -35.97 & -23.97 & -11.96 & 0.04 & 12.04 & 21.04 & 33.04 & 45.05 & 57.05 & 69.05  & dist. to img. \\ \hline
 & 228.04 & 219.04 & 207.04 & 195.04 & 183.03 & 171.03 & 162.03 & 150.03 & 138.03 & 126.02 & 114.02  & img. size \\ \hline
 & 0.05 & 0.02 & 0.03 & 0.12 & 0.40 & 0.68 & 0.77 & 0.90 & 0.95 & 0.90 & 0.85  & voting ratio \\ \hline
 & 9.45 & 8.50 & 4.95 & 4.95 & 2.96 & 4.95 & 7.56 & 12.11 & 12.10 & 105.25 & 114.39  & dist. true and detect. \\ \hline \hline
1024 & -27.00 & -21.00 & -12.00 & -6.00 & 3.00 & 9.00 & 18.00 & 24.00 & 33.00 & 39.00 & 48.00  & dist. to img. \\ \hline
 & 150.00 & 144.00 & 135.00 & 129.00 & 120.00 & 114.00 & 105.00 & 99.00 & 90.00 & 84.00 & 75.00  & img. size \\ \hline
 & 0.00 & 0.02 & 0.06 & 0.14 & 0.50 & 0.93 & 1.00 & 0.97 & 0.92 & 0.85 & 0.75  & voting ratio \\ \hline
 & 4.18 & 6.27 & 6.19 & 6.50 & 6.88 & 6.27 & 8.78 & 8.55 & 8.61 & 8.61 & 8.55  & dist. true and detect. \\ \hline \hline
1031 & -18.81 & -10.81 & -2.81 & 5.19 & 13.19 & 21.19 & 29.19 & 37.19 & 45.19 & 53.19 & 62.19  & dist. to img. \\ \hline
 & 163.00 & 155.00 & 147.00 & 139.00 & 131.00 & 123.00 & 115.00 & 107.00 & 99.00 & 91.00 & 82.00  & img. size \\ \hline
 & 0.07 & 0.16 & 0.39 & 1.11 & 1.53 & 1.50 & 1.39 & 1.25 & 1.11 & 1.02 & 0.92  & voting ratio \\ \hline
 & 3.85 & 3.27 & 3.27 & 2.06 & 2.06 & 4.52 & 4.52 & 4.52 & 4.52 & 4.52 & 3.98  & dist. true and detect. \\ \hline \hline
1043 & -22.65 & -13.95 & -4.95 & 3.75 & 12.75 & 21.75 & 30.45 & 39.45 & 48.15 & 57.15 & 66.15  & dist. to img. \\ \hline
 & 177.60 & 168.90 & 159.90 & 151.20 & 142.20 & 133.20 & 124.50 & 115.50 & 106.80 & 97.80 & 88.80  & img. size \\ \hline
 & 0.06 & 0.14 & 0.31 & 0.64 & 1.02 & 1.17 & 1.19 & 1.13 & 1.07 & 1.02 & 0.98  & voting ratio \\ \hline
 & 2.84 & 3.23 & 8.63 & 4.07 & 5.01 & 7.42 & 7.24 & 8.41 & 8.05 & 7.62 & 7.31  & dist. true and detect. \\ \hline \hline
1048 & -22.48 & -14.08 & -5.68 & 2.72 & 11.12 & 19.82 & 28.22 & 36.62 & 45.02 & 53.42 & 62.12  & dist. to img. \\ \hline
 & 169.19 & 160.79 & 152.39 & 143.99 & 135.59 & 126.89 & 118.50 & 110.10 & 101.70 & 93.30 & 84.60  & img. size \\ \hline
 & 0.04 & 0.12 & 0.33 & 0.92 & 1.54 & 1.52 & 1.40 & 1.27 & 1.08 & 1.01 & 0.91  & voting ratio \\ \hline
 & 3.26 & 2.86 & 6.36 & 3.94 & 6.04 & 5.98 & 5.98 & 6.87 & 6.68 & 11.78 & 11.40  & dist. true and detect. \\ \hline \hline
1056 & -12.70 & -7.30 & -1.90 & 3.50 & 9.20 & 14.60 & 20.00 & 25.70 & 31.10 & 36.50 & 42.20  & dist. to img. \\ \hline
 & 109.80 & 104.40 & 99.00 & 93.60 & 87.90 & 82.50 & 77.10 & 71.40 & 66.00 & 60.60 & 54.90  & img. size \\ \hline
 & 0.13 & 0.21 & 0.35 & 0.60 & 0.79 & 0.77 & 0.68 & 0.64 & 0.54 & 0.50 & 0.45  & voting ratio \\ \hline
 & 2.92 & 2.92 & 3.41 & 4.51 & 3.08 & 3.85 & 2.84 & 3.31 & 4.20 & 6.86 & 10.99  & dist. true and detect. \\ \hline \hline
1058 & -51.23 & -40.43 & -29.23 & -18.03 & -6.83 & 4.37 & 15.57 & 26.77 & 37.97 & 49.17 & 60.37  & dist. to img. \\ \hline
 & 223.60 & 212.80 & 201.60 & 190.40 & 179.20 & 168.00 & 156.80 & 145.60 & 134.40 & 123.20 & 112.00  & img. size \\ \hline
 & 0.00 & 0.00 & 0.00 & 0.01 & 0.13 & 0.69 & 1.43 & 1.85 & 1.84 & 1.64 & 1.40  & voting ratio \\ \hline
 & 5.00 & 5.85 & 6.47 & 7.92 & 7.82 & 10.30 & 9.98 & 9.73 & 8.75 & 15.52 & 15.06  & dist. true and detect. \\ \hline \hline
1059 & -11.23 & -2.83 & 5.87 & 14.27 & 22.97 & 31.67 & 40.07 & 48.77 & 57.17 & 65.87 & 74.57  & dist. to img. \\ \hline
 & 171.60 & 163.20 & 154.50 & 146.10 & 137.40 & 128.70 & 120.30 & 111.60 & 103.20 & 94.50 & 85.80  & img. size \\ \hline
 & 0.12 & 0.44 & 1.28 & 1.69 & 1.72 & 1.63 & 1.48 & 1.33 & 1.19 & 1.05 & 0.90  & voting ratio \\ \hline
 & 1.60 & 3.16 & 2.55 & 4.10 & 6.25 & 5.70 & 9.07 & 8.47 & 11.69 & 11.05 & 10.41  & dist. true and detect. \\ \hline \hline
1074 & -18.95 & -9.75 & -0.55 & 8.65 & 17.84 & 27.44 & 36.64 & 45.84 & 55.04 & 64.24 & 73.84  & dist. to img. \\ \hline
 & 185.58 & 176.38 & 167.18 & 157.99 & 148.79 & 139.19 & 129.99 & 120.79 & 111.59 & 102.39 & 92.79  & img. size \\ \hline
 & 0.00 & 0.02 & 0.12 & 0.46 & 1.14 & 1.58 & 1.60 & 1.48 & 1.33 & 1.19 & 1.01  & voting ratio \\ \hline
 & 2.52 & 7.61 & 4.39 & 13.17 & 13.99 & 12.42 & 13.17 & 13.99 & 12.18 & 12.91 & 13.99  & dist. true and detect. \\ \hline \hline
1079 & -18.17 & -8.57 & 1.03 & 10.63 & 20.63 & 30.23 & 39.83 & 49.83 & 59.43 & 69.03 & 79.03  & dist. to img. \\ \hline
 & 194.80 & 185.20 & 175.60 & 166.00 & 156.00 & 146.40 & 136.80 & 126.80 & 117.20 & 107.60 & 97.60  & img. size \\ \hline
 & 0.04 & 0.18 & 0.61 & 1.49 & 1.92 & 1.95 & 1.80 & 1.67 & 1.42 & 1.25 & 1.03  & voting ratio \\ \hline
 & 3.58 & 2.90 & 6.07 & 6.53 & 6.02 & 6.38 & 7.10 & 8.64 & 17.86 & 14.40 & 17.29  & dist. true and detect. \\ \hline \hline
1092 & -37.90 & -30.10 & -22.30 & -14.50 & -6.70 & 1.10 & 8.90 & 16.70 & 24.50 & 32.30 & 40.10  & dist. to img. \\ \hline
 & 156.29 & 148.49 & 140.69 & 132.89 & 125.09 & 117.30 & 109.50 & 101.70 & 93.90 & 86.10 & 78.30  & img. size \\ \hline
 & 0.01 & 0.04 & 0.09 & 0.21 & 0.44 & 0.91 & 1.15 & 1.07 & 0.98 & 0.94 & 0.84  & voting ratio \\ \hline
 & 2.28 & 2.45 & 2.62 & 2.80 & 8.16 & 4.80 & 5.60 & 5.72 & 5.83 & 9.36 & 11.68  & dist. true and detect. \\ \hline \hline
1094 & -21.35 & -11.75 & -1.75 & 8.25 & 17.85 & 27.84 & 37.84 & 47.44 & 57.44 & 67.44 & 77.44  & dist. to img. \\ \hline
 & 197.58 & 187.98 & 177.98 & 167.98 & 158.39 & 148.39 & 138.39 & 128.79 & 118.79 & 108.79 & 98.79  & img. size \\ \hline
 & 0.04 & 0.22 & 0.82 & 1.84 & 2.09 & 2.04 & 1.94 & 1.72 & 1.56 & 1.29 & 1.12  & voting ratio \\ \hline
 & 3.25 & 2.97 & 3.44 & 7.52 & 6.87 & 7.73 & 6.87 & 7.95 & 9.25 & 7.96 & 7.00  & dist. true and detect. \\ \hline \hline
    
    \hline
    \end{tabular}
\end{table*}

\end{document}